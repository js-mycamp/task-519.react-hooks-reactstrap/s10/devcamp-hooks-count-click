import { useEffect } from "react";
import { useState } from "react"

function Count(){
    const [count, setCount] = useState(0);

    const increaseCount = () => {
     //  console.log("increase Count")
        setCount(count + 1)
    };
    useEffect(() => {
        console.log("componentDidUpdate");
        document.title = `you clicked ${count} time`;
        return () => {
            console.log("componentWillUnmount")
        }
    });
    useEffect(() => {
        console.log("componentDidMount");
        document.title = `you clicked ${count} time`;
    }, [])
    return(
        <>
        <p>You clicked {count} times!</p>
        <button onClick={increaseCount}>Click me</button>
            </>
    )
}

export default Count